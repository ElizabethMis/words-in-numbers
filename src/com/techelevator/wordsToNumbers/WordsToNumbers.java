package com.techelevator.wordsToNumbers;

import java.util.HashMap;
import java.util.Map;

public class WordsToNumbers {

	public static int wordsInNumbers(String number) {
		
		int hundredsValue = 0;
		int thousandsValue = 0;
		
		if (number.equals("Zero")) {
			return hundredsValue;
		}
		
		Map<String, Integer> wordsInNumbers = wordsToNumbers();
		
		
		String[] separatedNumberWords = number.split(" ");
		for (String s : separatedNumberWords) {
			if (s.equals("Thousand")) {
				thousandsValue = hundredsValue * 1000;
				hundredsValue = 0;
			} else if (s.equals("Hundred")) {
				hundredsValue *= 100;
			} else {
				hundredsValue += wordsInNumbers.get(s);
		
			}
		}
		return hundredsValue + thousandsValue;	
	}

	private static Map<String, Integer> wordsToNumbers() {
		Map<String, Integer> wordsInNumbers = new HashMap<String, Integer>();
		wordsInNumbers.put("One", 1);
		wordsInNumbers.put("Two", 2);
		wordsInNumbers.put("Three", 3);
		wordsInNumbers.put("Four", 4);
		wordsInNumbers.put("Five", 5);
		wordsInNumbers.put("Six", 6);
		wordsInNumbers.put("Seven", 7);
		wordsInNumbers.put("Eight", 8);
		wordsInNumbers.put("Nine", 9);
		wordsInNumbers.put("Ten", 10);
		wordsInNumbers.put("Eleven", 11);
		wordsInNumbers.put("Twelve", 12);
		wordsInNumbers.put("Thirteen", 13);
		wordsInNumbers.put("Fourteen", 14);
		wordsInNumbers.put("Fifteen", 15);
		wordsInNumbers.put("Sixteen", 16);
		wordsInNumbers.put("Seventeen", 17);
		wordsInNumbers.put("Eighteen", 18);
		wordsInNumbers.put("Nineteen", 19);
		wordsInNumbers.put("Twenty", 20);
		wordsInNumbers.put("Thirty", 30);
		wordsInNumbers.put("Forty", 40);
		wordsInNumbers.put("Fifty", 50);
		wordsInNumbers.put("Sixty", 60);
		wordsInNumbers.put("Seventy", 70);
		wordsInNumbers.put("Eighty", 80);
		wordsInNumbers.put("Ninety", 90);
		wordsInNumbers.put("Hundred", 100);
		wordsInNumbers.put("Thousand", 1000);
		return wordsInNumbers;
	}

}
