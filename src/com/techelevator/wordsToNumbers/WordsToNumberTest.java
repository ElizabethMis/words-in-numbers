package com.techelevator.wordsToNumbers;

import static org.junit.Assert.*;
import org.junit.Test;

public class WordsToNumberTest {
	
	@Test
	public void returns_single_didgit_number_from_string() {
		assertEquals(1, WordsToNumbers.wordsInNumbers("One"));
		assertEquals(2, WordsToNumbers.wordsInNumbers("Two"));
		assertEquals(3, WordsToNumbers.wordsInNumbers("Three"));
		assertEquals(4, WordsToNumbers.wordsInNumbers("Four"));
		assertEquals(5, WordsToNumbers.wordsInNumbers("Five"));
		assertEquals(6, WordsToNumbers.wordsInNumbers("Six"));
		assertEquals(7, WordsToNumbers.wordsInNumbers("Seven"));
		assertEquals(8, WordsToNumbers.wordsInNumbers("Eight"));
		assertEquals(9, WordsToNumbers.wordsInNumbers("Nine"));
	}	
	
	@Test
	public void returns_double_digit_from_10_to_20() {
		assertEquals(10, WordsToNumbers.wordsInNumbers("Ten"));
		assertEquals(11, WordsToNumbers.wordsInNumbers("Eleven"));
		assertEquals(15, WordsToNumbers.wordsInNumbers("Fifteen"));
		assertEquals(19, WordsToNumbers.wordsInNumbers("Nineteen"));
		assertEquals(20, WordsToNumbers.wordsInNumbers("Twenty"));
		assertEquals(22, WordsToNumbers.wordsInNumbers("Twenty Two"));
	}
	
	@Test
	public void returns_double_digit_from_40_50() {
		assertEquals(40, WordsToNumbers.wordsInNumbers("Forty"));
		assertEquals(45, WordsToNumbers.wordsInNumbers("Forty Five"));
		assertEquals(50, WordsToNumbers.wordsInNumbers("Fifty"));
		assertEquals(55, WordsToNumbers.wordsInNumbers("Fifty Five"));
	}
	
	@Test
	public void returns_double_digit_from_90_to_99() {
		assertEquals(90, WordsToNumbers.wordsInNumbers("Ninety"));
		assertEquals(91, WordsToNumbers.wordsInNumbers("Ninety One"));
		assertEquals(95, WordsToNumbers.wordsInNumbers("Ninety Five"));
		assertEquals(99, WordsToNumbers.wordsInNumbers("Ninety Nine"));
	}
	
	@Test
	public void returns_triple_didgit_from_100_to_109() {
 		assertEquals(100, WordsToNumbers.wordsInNumbers("One Hundred"));
		assertEquals(101, WordsToNumbers.wordsInNumbers("One Hundred One"));
		assertEquals(105, WordsToNumbers.wordsInNumbers("One Hundred Five"));
		assertEquals(109, WordsToNumbers.wordsInNumbers("One Hundred Nine"));	
	}
	
	@Test
	public void returns_triple_didgits_from_300_450() {
		assertEquals(300, WordsToNumbers.wordsInNumbers("Three Hundred"));
		assertEquals(324, WordsToNumbers.wordsInNumbers("Three Hundred Twenty Four"));
		assertEquals(357, WordsToNumbers.wordsInNumbers("Three Hundred Fifty Seven"));
		assertEquals(372, WordsToNumbers.wordsInNumbers("Three Hundred Seventy Two"));
		assertEquals(391, WordsToNumbers.wordsInNumbers("Three Hundred Ninety One"));
		assertEquals(408, WordsToNumbers.wordsInNumbers("Four Hundred Eight"));
		assertEquals(436, WordsToNumbers.wordsInNumbers("Four Hundred Thirty Six"));
		assertEquals(444, WordsToNumbers.wordsInNumbers("Four Hundred Forty Four"));
	}
	
	@Test
	public void returns_four_digits_from_1000_to_5050() {
		assertEquals(1000, WordsToNumbers.wordsInNumbers("One Thousand"));
		assertEquals(1097, WordsToNumbers.wordsInNumbers("One Thousand Ninety Seven"));
		assertEquals(2888, WordsToNumbers.wordsInNumbers("Two Thousand Eight Hundred Eighty Eight"));
		assertEquals(2901, WordsToNumbers.wordsInNumbers("Two Thousand Nine Hundred One"));
		assertEquals(3555, WordsToNumbers.wordsInNumbers("Three Thousand Five Hundred Fifty Five"));
		assertEquals(3789, WordsToNumbers.wordsInNumbers("Three Thousand Seven Hundred Eighty Nine"));
		assertEquals(3999, WordsToNumbers.wordsInNumbers("Three Thousand Nine Hundred Ninety Nine"));
		assertEquals(4009, WordsToNumbers.wordsInNumbers("Four Thousand Nine"));
		assertEquals(4505, WordsToNumbers.wordsInNumbers("Four Thousand Five Hundred Five"));
		assertEquals(5050, WordsToNumbers.wordsInNumbers("Five Thousand Fifty"));
	}	

}
